public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        
        //3. Print the elements of the "numbers" array using a for loop.
        for(int i=0; i<numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }
        System.out.println();

        //4. Print the elements of the "names" array using a for-each loop.
        for (String n : names) {
            System.out.print(n+" ");
        }
        System.out.println();

        //5. Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0] = 1.18;
        values[1] = 8.36;
        values[2] = 14.98;
        values[3] = 9.59;

        //6. Calculate and print the sum of all elements in the "numbers" array.
        int sum = 0;
        for (int number : numbers) {
            sum =sum + number;
        }
        System.out.println("The sum of all elements in the numbers : " + sum);

        //7. Find and print the maximum value in the "values" array.
        double max = values[0];
        for(int i=0; i<values.length; i++) {
            if(values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("the maximum value in the 'values' : "+ max);

        //8. Create a new string array named "reversedNames" with the same length as the "names" array.
            //Create a new string array named "reversedNames" with the same length as the "names" array.
            //Print the elements of the "reversedNames" array.
        String[] reversedNames = new String[names.length];
        for(int i=0; i<names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        for(int i=0; i<reversedNames.length; i++) {
            System.out.print(reversedNames[i]+" ");
        }

    }
}
